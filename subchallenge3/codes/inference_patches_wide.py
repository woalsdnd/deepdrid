# python inference_patches_wide.py --gpu_index=0,2 --img_dir=../../data/DeepDRiD/ultra-widefield-training/images/ --out_dir=../intermediate_outputs/train --phase=train
# python inference_patches_wide.py --gpu_index=0,2 --img_dir=../../data/DeepDRiD/Online-Challenge/ultra-widefield/ --out_dir=../intermediate_outputs/offsite --phase=test
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils

from model.efficient_net import EfficientNetB4, set_output
from keras.models import Model
from keras.utils.training_utils import multi_gpu_model

PATCH_SIZE = 1024
INPUT_STRIDE = PATCH_SIZE // 2
N_PATCH_HORIZONTAL = 7
N_PATCH_VERTICAL = 5

WIDTH_ENTIRE_IMG = PATCH_SIZE + (N_PATCH_HORIZONTAL-1) * INPUT_STRIDE
HEIGHT_ENTIRE_IMG = PATCH_SIZE + (N_PATCH_VERTICAL-1) * INPUT_STRIDE

def threshold(val):
    threhsolds = [0.5125, 1.425,  2.5625, 3.5875]
    if val < threhsolds[0]:
        label = 0
    elif val >= threhsolds[0] and val < threhsolds[1]:
        label = 1
    elif val >= threhsolds[1] and val < threhsolds[2]:
        label = 2
    elif val >= threhsolds[2] and val < threhsolds[3]:
        label = 3
    else:
        label = 4
    return label

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--phase',
    type=str,
    choices=["train", "optimize_thresholds", "offsite", "onsite"],
    required=True
    )  
FLAGS, _ = parser.parse_known_args()

# misc params
list_weight_path = ["/home/vuno/development/DeepDRiD/model/B4/iteration_ultra-wide/overfit_val_weight_{}epoch.h5".format(epoch) for epoch in range(4, 8)]
input_size = (PATCH_SIZE, PATCH_SIZE, 3)  # (h,w)
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
n_gpus = len(FLAGS.gpu_index.split(","))
out_dir = FLAGS.out_dir
utils.makedirs(out_dir)

# run inference
if FLAGS.phase != "optimize_thresholds":
    # load a network
    list_network = []
    for weight_path in list_weight_path:
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network = set_output(network)
        if n_gpus>1:
            network = multi_gpu_model(network, gpus=n_gpus)
        network.load_weights(weight_path)
        print("model loaded from {}".format(weight_path))
        list_network.append(network)

    fpaths = utils.all_files_under(FLAGS.img_dir)
    for fpath in fpaths:
        print("processing {}...".format(fpath))
        wide_fundus_img = np.array(Image.open(fpath))
        wide_h, wide_w, _ = wide_fundus_img.shape
        ori_img = np.zeros((HEIGHT_ENTIRE_IMG, WIDTH_ENTIRE_IMG, 3))
        ori_img[(HEIGHT_ENTIRE_IMG - wide_h) // 2:(HEIGHT_ENTIRE_IMG - wide_h) // 2 + wide_h, (WIDTH_ENTIRE_IMG - wide_w) // 2 :(WIDTH_ENTIRE_IMG - wide_w) // 2 + wide_w, :] = wide_fundus_img
        ori_h, ori_w, _ = ori_img.shape
        h_center = ori_h // 2
        w_center = ori_w // 2
        x_start = w_center - WIDTH_ENTIRE_IMG // 2
        y_start = h_center - HEIGHT_ENTIRE_IMG // 2
        
        # prepare aug images (horizontal, vertical flips)
        ori_img = 1.*ori_img / 255
        horizontal_flip = np.copy(ori_img)
        horizontal_flip = horizontal_flip[:,::-1,:]
        vertical_flip = np.copy(ori_img)
        vertical_flip = vertical_flip[::-1,:,:]
        horizontal_vertical_flip = np.copy(ori_img)
        horizontal_vertical_flip = horizontal_vertical_flip[::-1,::-1,:]

        # run inference for each patch
        for aug_type, img in zip(["ori","horizontal", "vertical","horizontal_vertical"], [ori_img, horizontal_flip, vertical_flip, horizontal_vertical_flip]):
            result_arr = np.zeros((N_PATCH_VERTICAL, N_PATCH_HORIZONTAL))
            for index_y in range(N_PATCH_VERTICAL):
                for index_x in range(N_PATCH_HORIZONTAL):
                    if index_y==2 or (index_y==1 and index_x==3) or (index_y==3 and index_x==3):
                        x_s = x_start + INPUT_STRIDE * index_x
                        y_s = y_start + INPUT_STRIDE * index_y
                        patch = img[y_s:y_s+PATCH_SIZE, x_s:x_s+PATCH_SIZE,:]
                        network_input = np.expand_dims(patch, axis=0)
                        outputs=[]
                        for index, network in enumerate(list_network):
                            pred = np.clip(network.predict(network_input)[-1][0,0], 0, 4)
                            outputs.append(pred)
                        result_arr[index_y, index_x] = np.mean(outputs)
                        print(index_y, index_x, np.mean(outputs))
            np.save(os.path.join(out_dir, "{}-{}.npy".format(os.path.basename(fpath).split(".")[0], aug_type)), result_arr)

if FLAGS.phase!="train":
    array_fpaths = utils.all_files_under(out_dir)
    
    # load network
    from model.efficient_net import mixing_layer
    dir_save_weights = "/home/vuno/development/DeepDRiD/model/patch_training"
    h_arr, w_arr = np.load(array_fpaths[0]).shape
    network = mixing_layer(h_arr, w_arr, 0)
    network.load_weights(os.path.join(dir_save_weights, "patch_training_weights.h5"))
    
    # run inference
    list_image_id, list_level = [], []
    df_fpath = pd.DataFrame({"fpath":array_fpaths, "image_id":[os.path.basename(array_fpath).split("-")[0] for array_fpath in array_fpaths]})
    for image_id, group in df_fpath.groupby("image_id"):
        list_image_id.append(image_id)
        outputs = []
        for fpath in group["fpath"]:
            input_arr = np.zeros((1, h_arr, w_arr, 1))
            input_arr[0,...,0] = np.load(fpath)
            output = network.predict(input_arr)[0,0]
            outputs.append(np.clip(output, 0, 4))
        level = np.mean(outputs) if FLAGS.phase == "optimize_thresholds" else threshold(np.mean(outputs))
        list_level.append(level)

    df_result = pd.DataFrame({"image_id": list_image_id, "DR_level": list_level})
    csv_submission_fname = "Challenge3_upload.csv"
    if FLAGS.phase =="offsite":
        df_submission_template = pd.read_csv(os.path.join("/home/vuno/development/data/DeepDRiD/Online-Challenge", csv_submission_fname))
    elif FLAGS.phase =="onsite":
        df_submission_template = pd.read_csv(os.path.join("/home/vuno/development/data/DeepDRiD/Onsite-Challenge", csv_submission_fname))
    df_submission = pd.merge(df_submission_template[["image_id"]], df_result, on="image_id")
    df_submission[["image_id", "DR_level"]].to_csv(os.path.join(out_dir, "Subchallenge3.csv"), index=False)