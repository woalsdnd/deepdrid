# python compare_post_processing.py --gt_csv=../../data/DeepDRiD/ultra-widefield-training/ultra-widefield.csv  --pred_csv=../submission/epoch0_wide_train/Training_result.csv --img_type=wide > ../submission/epoch0_wide_train/out_performance
# python compare_post_processing.py --gt_csv=../../data/DeepDRiD/regular-fundus-training/regular-fundus.csv  --pred_csv=../submission/epoch1_regular_train/Training_result.csv --img_type=regular
import os
import pandas as pd
import utils
import numpy as np
import argparse


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--pred_csv',
    type=str,
    required=True
)
parser.add_argument(
    '--gt_csv',
    type=str,
    required=True
)
parser.add_argument(
    '--img_type',
    type=str,
    choices=['regular', 'wide'],
    required=True
)
FLAGS, _ = parser.parse_known_args()

df_pred = pd.read_csv(FLAGS.pred_csv)
df_pred["image_id"] = df_pred["image_id"].map(lambda x:x.split(".")[0])
df_gt = pd.read_csv(FLAGS.gt_csv)
if FLAGS.img_type=="regular":
    df_gt["gt"] = df_gt.apply(lambda row: row["left_eye_DR_Level"] if "l" in row["image_id"] else row["right_eye_DR_Level"], axis=1)
elif FLAGS.img_type=="wide":
    df_gt["gt"] = df_gt["DR_level"]
    df_gt = df_gt.loc[df_gt["gt"]<5,:]
col_name = "DR_level" if FLAGS.img_type=="wide" else "DR_Level"
df_pred[col_name] = df_pred[col_name].map(lambda x: np.round(x))
df_merged = pd.merge(df_gt[["image_id","gt"]], df_pred, on="image_id")
print("===== no post processing =====")
print(utils.categorical_stats(df_merged["gt"], df_merged[col_name], weighted_kappa=True))
print("===== mean =====")
df_pred.loc[:,"img_lat"] = df_pred["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
df_pred_mean = df_pred.groupby("img_lat")[col_name].apply(lambda x:np.round(np.mean(x))).reset_index()
df_pred_mean_merged = pd.merge(df_pred[["img_lat","image_id"]], df_pred_mean, on="img_lat")
df_merged = pd.merge(df_gt[["image_id","gt"]], df_pred_mean_merged, on="image_id")
print(utils.categorical_stats(df_merged["gt"], df_merged[col_name], weighted_kappa=True))
print("===== max =====")
df_pred_max = df_pred.groupby("img_lat")[col_name].apply(lambda x:np.max(x)).reset_index()
df_pred_max_merged = pd.merge(df_pred[["img_lat","image_id"]], df_pred_max, on="img_lat")
df_merged = pd.merge(df_gt[["image_id","gt"]], df_pred_max_merged, on="image_id")
print(utils.categorical_stats(df_merged["gt"], df_merged[col_name], weighted_kappa=True))