import sys
import os
import argparse
import logging

import numpy as np
import pandas as pd

import utils
import iterator_shared_array
from model.efficient_net import mixing_layer
from keras import backend as K

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
)
FLAGS, _ = parser.parse_known_args()
n_epochs = 3000
lr_start_value = 1e-3
lr_decay_factor = 1e-1
path_logger = "/home/vuno/development/DeepDRiD/log/patch_training.log"
dir_save_weights = "/home/vuno/development/DeepDRiD/model/patch_training"
os.makedirs(dir_save_weights, exist_ok=True)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)

# prepare data
dir_array="/home/vuno/development/DeepDRiD/intermediate_outputs/train"
array_fpaths = utils.all_files_under(dir_array)
df_fpath = pd.DataFrame({"fpath":array_fpaths, "image_id":[os.path.basename(array_fpath).split("-")[0].replace(".jpg", "") for array_fpath in array_fpaths]})
df_label = pd.read_csv("/home/vuno/development/data/DeepDRiD/ultra-widefield-training/ultra-widefield.csv")
df_merged = pd.merge(df_fpath, df_label[["image_id","DR_level"]], on="image_id")
df_merged = df_merged.dropna()
h_arr, w_arr = np.load(array_fpaths[0]).shape
input_arr = np.zeros((len(array_fpaths), h_arr, w_arr, 1))
label_arr = np.array(list(df_merged["DR_level"]))
for index, fpath in enumerate(df_merged["fpath"]):
    array_fpath = array_fpaths[index]
    input_arr[index,...,0] = np.load(array_fpath)

# train network
network = mixing_layer(h_arr, w_arr, lr_start_value)
lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=1e-6, lr_decay_tolerance=1000,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x)
for epoch in range(n_epochs):
    loss = network.train_on_batch(input_arr, label_arr)
    logger.info("training loss at {}th epoch: {}".format(epoch, loss))
    lr_scheduler.adjust_lr(epoch, loss)
np.save(os.path.join(dir_save_weights, "conv_weight.npy"), network.layers[1].get_weights())
network.save_weights(os.path.join(dir_save_weights, "patch_training_weights.h5"))