for iter_loop in 1 2 3 4 5 6 7 8 9; do
    python3 train_ultra_wide_overfit_val.py  --config_file=../config/iteration_B4_1024x1024_ultra_wide.cfg --iter_loop=${iter_loop}
    python3 compute_ultra_wide_pseudo_label.py  --config_file=../config/iteration_B4_1024x1024_ultra_wide.cfg --iter_loop=${iter_loop}
    python3 train_ultra_wide_loop.py  --config_file=../config/iteration_B4_1024x1024_ultra_wide.cfg --iter_loop=${iter_loop}
done    