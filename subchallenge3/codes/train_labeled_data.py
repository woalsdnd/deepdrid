import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB2, EfficientNetB3, EfficientNetB4, set_output, compile_loss

from keras.utils.training_utils import multi_gpu_model


def label2binary(gt_labels):
    gt_step0 = np.array([0 if gt <= 0 else 1 for gt in gt_labels])
    gt_step1 = np.array([0 if gt <= 1 else 1 for gt in gt_labels])
    gt_step2 = np.array([0 if gt <= 2 else 1 for gt in gt_labels])
    gt_step3 = np.array([0 if gt <= 3 else 1 for gt in gt_labels])
    return gt_step0, gt_step1, gt_step2, gt_step3


def set_filenames_labels(dataset_name, fpaths_data, df_label):
   if "kaggle2015" in dataset_name:
       fid_col, label_col = "image", "level"
   elif "APTOS" in dataset_name:
       fid_col, label_col = "id_code", "diagnosis"
   elif "IDRiD" in dataset_name:
       fid_col, label_col = "Image name", "Retinopathy grade"
   elif "DeepDRiD" in dataset_name:
       df_label["level"] = df_label.apply(lambda row:row["left_eye_DR_Level"] if "l" in row["image_id"] else row["right_eye_DR_Level"], axis=1)
       fid_col, label_col = "image_id", "level"
   df_filepath_idcode = pd.DataFrame({"filepath":fpaths_data, fid_col:[os.path.basename(fpath).replace(".png", "") for fpath in fpaths_data]})
   df_merged = pd.merge(df_label, df_filepath_idcode, on=fid_col, how="inner")
   return list(df_merged["filepath"]), list(df_merged[label_col])


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
dir_training_input_data2 = config["Path"]["path_data2"]
dir_training_input_data3 = config["Path"]["path_data3"]
dir_training_input_label1 = config["Path"]["path_label1"]
dir_training_input_label2 = config["Path"]["path_label2"]
dir_training_input_label3 = config["Path"]["path_label3"]
dir_val_input_data = config["Path"]["path_val_data"]
dir_val_input_label = config["Path"]["path_val_label"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
model_type = config["Train"]["model_type"]
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# split data
fpaths_data1 = utils.all_files_under(dir_training_input_data1)
fpaths_data2 = utils.all_files_under(dir_training_input_data2)
fpaths_data3 = utils.all_files_under(dir_training_input_data3)
fpaths_val_data = utils.all_files_under(dir_val_input_data)
df_label_data1 = pd.read_csv(dir_training_input_label1)
df_label_data2 = pd.read_csv(dir_training_input_label2)
df_label_data3 = pd.read_csv(dir_training_input_label3)
df_val_label = pd.read_csv(dir_val_input_label)
# set filenames and labels
fpaths_data1, label_data1 = set_filenames_labels(dir_training_input_data1, fpaths_data1, df_label_data1) 
fpaths_data2, label_data2 = set_filenames_labels(dir_training_input_data2, fpaths_data2, df_label_data2)
fpaths_data3, label_data3 = set_filenames_labels(dir_training_input_data3, fpaths_data3, df_label_data3)    
fpaths_val_data, label_val_data = set_filenames_labels(dir_val_input_data, fpaths_val_data, df_val_label)    
assert len(fpaths_data1) == len(label_data1)
assert len(fpaths_data2) == len(label_data2)
assert len(fpaths_data3) == len(label_data3)
assert len(fpaths_val_data) == len(label_val_data)

# set iterator
class_weight, sample_weight_data1 = utils.balanced_class_weights(label_data1)
logger.info("{} - class weight: {}".format(dir_training_input_data1, class_weight))
class_weight, sample_weight_data2 = utils.balanced_class_weights(label_data2)
logger.info("{} - class weight: {}".format(dir_training_input_data2, class_weight))
class_weight, sample_weight_data3 = utils.balanced_class_weights(label_data3)
logger.info("{} - class weight: {}".format(dir_training_input_data3, class_weight))
list_sample_weights = [sample_weight_data1, sample_weight_data2, sample_weight_data3]
train_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_data1, label_data1, fpaths_data2, label_data2, fpaths_data3, label_data3), list_sample_weights,
                                           utils.fundus_classification_processing_func_train_DeepDRiD, batch_size, sample=True, replace=True,
                                           shared_array_shape=[input_size, (), input_size, (), input_size, ()])
val_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_val_data, label_val_data), None,
                                          utils.fundus_classification_processing_func_val_DeepDRiD, batch_size, sample=False, replace=False, shared_array_shape=[input_size, ()])

# define network
# make imagenet-pretrained if not exists
if "imagenet_pretrained" in path_load_model and (not os.path.exists(path_load_model) or len(utils.all_files_under(path_load_model, extension="h5")) == 0):
    from keras.applications.resnet50 import ResNet50
    resnet = ResNet50(weights='imagenet')
    if model_type == "B2":
        network = EfficientNetB2(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB2(include_top=False, weights="imagenet", input_shape=input_size)
    elif model_type == "B3":
        network = EfficientNetB3(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB3(include_top=False, weights="imagenet", input_shape=input_size)
    elif model_type == "B4":
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB4(include_top=False, weights="imagenet", input_shape=input_size)
    network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    network = utils.copy_weights(network_imagenet_pretrained, network)
    utils.makedirs(path_load_model)
    network.save_weights(os.path.join(path_load_model, "imagenet_pretrained_weight.h5"))
else:
    if model_type == "B2":
        network = EfficientNetB2(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    elif model_type == "B3":
        network = EfficientNetB3(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    elif model_type == "B4":
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    network.load_weights(os.path.join(path_load_model, "imagenet_pretrained_weight.h5"))
 
# compile network
ratio_n_dataset = utils.count2ratio([len(fpaths_data1), len(fpaths_data2), len(fpaths_data3)])
loss_weights = np.concatenate([np.array([0.1 * ratio] * 4 + [ratio]) for ratio in ratio_n_dataset])
network_data1 = set_output(network)
network_data2 = set_output(network)
network_data3 = set_output(network)
network_data1 = multi_gpu_model(network_data1, gpus=2)
network_data2 = multi_gpu_model(network_data2, gpus=2)
network_data3 = multi_gpu_model(network_data3, gpus=2)
network_data1 = compile_loss(network_data1, lr_start_value, loss_weights[0])
network_data2 = compile_loss(network_data2, lr_start_value, loss_weights[1])
network_data3 = compile_loss(network_data3, lr_start_value, loss_weights[2])
tensorboard.set_model(network)  # set tensorboard callback associated with network

for epoch in range(n_epochs):
    # train loop
    dict_train_loss = {"{}_data{}".format(metric, data_num):[] for data_num in range(1, 4) for metric in ["loss_total", "loss0", "loss1", "loss2", "loss3", "loss_reg", "acc0", "acc1", "acc2", "acc3"]}
    for list_arr in train_batch_fetcher:
        # list_arr = [img1, label1, img2, label2, img3, label3]
        tensorboard.draw_imgs("Training Image", epoch, (np.concatenate([list_arr[0], list_arr[2], list_arr[4]], axis=0) * 255).astype(np.uint8), plot_once=True)
  
        gt_step0_data1, gt_step1_data1, gt_step2_data1, gt_step3_data1 = label2binary(list_arr[1])
        gt_step0_data2, gt_step1_data2, gt_step2_data2, gt_step3_data2 = label2binary(list_arr[3])
        gt_step0_data3, gt_step1_data3, gt_step2_data3, gt_step3_data3 = label2binary(list_arr[5])
        loss_total_data1, loss0_data1, loss1_data1, loss2_data1, loss3_data1, loss_reg_data1, acc0_data1, acc1_data1, acc2_data1, acc3_data1, _ = network_data1.train_on_batch(list_arr[0], [gt_step0_data1, gt_step1_data1, gt_step2_data1, gt_step3_data1, list_arr[1]])
        loss_total_data2, loss0_data2, loss1_data2, loss2_data2, loss3_data2, loss_reg_data2, acc0_data2, acc1_data2, acc2_data2, acc3_data2, _ = network_data2.train_on_batch(list_arr[2], [gt_step0_data2, gt_step1_data2, gt_step2_data2, gt_step3_data2, list_arr[3]])
        loss_total_data3, loss0_data3, loss1_data3, loss2_data3, loss3_data3, loss_reg_data3, acc0_data3, acc1_data3, acc2_data3, acc3_data3, _ = network_data3.train_on_batch(list_arr[4], [gt_step0_data3, gt_step1_data3, gt_step2_data3, gt_step3_data3, list_arr[5]])
          
        # store training loss, acc
        for data_num in range(1, 4):
            for metric in ["loss_total", "loss0", "loss1", "loss2", "loss3", "loss_reg", "acc0", "acc1", "acc2", "acc3"]:
                varname = "{}_data{}".format(metric, data_num)
                dict_train_loss[varname] += [locals()[varname]] * len(list_arr[0])

    train_metrics = {key:np.mean(val) for key, val in dict_train_loss.items()}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)
    # val loop
    list_gt, list_pred = [], []
    for list_arr in val_batch_fetcher:
        # list_arr = [img, label]
        tensorboard.draw_imgs("Validation Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        
        preds0, preds1, preds2, preds3, pred_reg = network_data2.predict(list_arr[0])
        utils.stack_list(head1=list_gt, tail1=list(list_arr[1]), head2=list_pred, tail2=list(utils.float2class(pred_reg, 0, 4)))
    val_metrics = utils.categorical_stats(list_gt, list_pred, weighted_kappa=True)
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, {key:val for key, val in val_metrics.items() if key != "confusion_matrix"})

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))