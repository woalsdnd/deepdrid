# python crop_resize.py  --img_dir=PALM-Training400/ --out_dir=preprocessed/PALM-Training400  --n_processes=30 --w_target=512 --h_target=512 --extension="jpg"
import argparse

from PIL import Image

import numpy as np
import os
import multiprocessing
from skimage import measure
from scipy.misc import imresize


def replace_img_format(file_basename, extension):
    file_basename = file_basename.replace("." + extension, ".png")
    return file_basename

    
def process(args):
    h_target, w_target, filenames, out_dir, extension = args
    for filename in filenames:
        # skip processed files
        out_file = os.path.join(out_dir, replace_img_format(os.path.basename(filename), extension))
        if os.path.exists(out_file):
            continue
        
        # read the image and resize
        im = Image.open(filename)
        img = np.array(im)
        
        # img = img[120:900,:] # wide-fundus
        img = img[160:880,160:880] # regular-fundus

        if len(img.shape)>=3 and img.shape[2] == 4:  # exclude alpha channel
            img = img[..., :3]
        
        if len(img.shape)>=3:
            resized_img = imresize(img, (h_target, w_target), 'bicubic')
        elif len(img.shape)==2:
            resized_img = imresize(img, (h_target, w_target), 'nearest')


        # save cropped image
        Image.fromarray(resized_img).save(out_file)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    help="directory of original images",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# divide files
all_fpaths = all_files_under(FLAGS.img_dir)
dict_mean = {"r":[], "g":[], "b":[]}
for fpath in all_fpaths:
    arr = np.array(Image.open(fpath))
    dict_mean["r"].append(np.mean(arr[...,0]))
    dict_mean["g"].append(np.mean(arr[...,1]))
    dict_mean["b"].append(np.mean(arr[...,2]))

print({color:np.mean(mean_val) for color, mean_val in dict_mean.items()})