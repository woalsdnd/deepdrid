import numpy as np
from PIL import Image
import argparse
import os
import multiprocessing

mean_regular = np.array([149.9898046924874, 90.47593851055778, 52.19136244500905])
mean_wide = np.array([85.39259348064661, 57.32777038216591, 0.6681374907493591])
ratio = mean_wide/mean_regular
# ratio[1]*=1.2

def process(args):
    filenames, out_dir = args
    for filename in filenames:
        # skip processed files
        out_file = os.path.join(out_dir, os.path.basename(filename))
        if os.path.exists(out_file):
            continue
        
        arr = np.array(Image.open(filename)).astype(np.float)
        for i in range(3):
            arr[...,i] = arr[...,i] * ratio[i]
        arr *= 1.4
        arr = np.clip(arr, 0, 255)

        os.makedirs(FLAGS.out_dir, exist_ok=True)
        Image.fromarray(arr.astype(np.uint8)).save(os.path.join(FLAGS.out_dir, os.path.basename(filename)))

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--n_processes',
    type=int,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

all_fpaths = all_files_under(FLAGS.img_dir)
filenames = [[] for __ in range(FLAGS.n_processes)]
chunk_sizes = len(all_fpaths) // FLAGS.n_processes
for index in range(FLAGS.n_processes):
    if index == FLAGS.n_processes - 1:  # allocate ranges (last GPU takes remainders)
        start, end = index * chunk_sizes, len(all_fpaths)
    else:
        start, end = index * chunk_sizes, (index + 1) * chunk_sizes
    filenames[index] = all_fpaths[start:end]

# run multiple processes
pool = multiprocessing.Pool(processes=FLAGS.n_processes)
pool.map(process, [(filenames[i], FLAGS.out_dir) for i in range(FLAGS.n_processes)])

