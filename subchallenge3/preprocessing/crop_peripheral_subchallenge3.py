# python crop_peripheral_subchallenge3.py --img_dir=../../data/DeepDRiD/ultra-widefield-training/images/ --out_dir=../../data/DeepDRiD/ultra-widefield-training/peripheral --n_processes=8 --extension=jpg
import argparse

from PIL import Image

import numpy as np
import pandas as pd
import os
import multiprocessing
from skimage import measure
from scipy.misc import imresize

LEN_SIDE = 1024
N_HORIZONTAL = 7
N_VERTICAL = 5

def replace_img_format(file_basename, crop_index, extension):
    file_id = file_basename.replace("." + extension, "")
    file_basename = file_id+"_{}".format(crop_index)+".png"
    return file_basename

    
def process(args):
    filenames, labels, out_dir, extension = args
    for index, filename in enumerate(filenames):
        label = labels[index]
        # read the image and resize
        im = Image.open(filename)
        img = np.array(im)
        if len(img.shape)<3:
            continue

        # crop the image 
        h, w, _ = img.shape
        h_step_size = (h - LEN_SIDE) // (N_VERTICAL-1)
        w_step_size = (w - LEN_SIDE) // (N_HORIZONTAL-1)
        for index_horizontal in range(N_HORIZONTAL):
            for index_vertical in range(N_VERTICAL):
                # skip processed files
                crop_index = index_horizontal * N_VERTICAL + index_vertical
                out_path = os.path.join(out_dir, replace_img_format(os.path.basename(filename), crop_index, extension))
                if os.path.exists(out_path):
                    continue
                w_start = w_step_size * index_horizontal
                h_start = h_step_size * index_vertical
                if label != 0 and w // 10 <= w_start + LEN_SIDE // 2 and 9 * w // 10 >= w_start + LEN_SIDE // 2 and h // 10 <= h_start + LEN_SIDE // 2 and 9 * h // 10 >= h_start + LEN_SIDE // 2:
                    continue
                cropped_image = img[h_start:h_start+LEN_SIDE, w_start:w_start+LEN_SIDE, :]
                Image.fromarray(cropped_image.astype(np.uint8)).save(out_path)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    help="directory of original images",
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    help="output directory of cropped images",
    required=True
    )
parser.add_argument(
    '--n_processes',
    type=int,
    help="Directory of output image files",
    required=True
    )
parser.add_argument(
    '--extension',
    type=str,
    help="Target height",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# make out_dir if not exists
if not os.path.isdir(FLAGS.out_dir):
    os.makedirs(FLAGS.out_dir)

# divide files
all_files = all_files_under(FLAGS.img_dir)
df = pd.read_csv("/home/vuno/development/data/DeepDRiD/ultra-widefield-training/ultra-widefield.csv")
list_image_id = [os.path.basename(fpath).replace(".jpg", "") for fpath in all_files]
df_fpath_id = pd.DataFrame({"image_id":list_image_id, "fpath":all_files})
df_merged = pd.merge(df[["image_id", "DR_level"]], df_fpath_id, on="image_id")
all_files = list(df_merged["fpath"])
all_labels = list(df_merged["DR_level"])
filenames = [[] for __ in range(FLAGS.n_processes)]
labels = [[] for __ in range(FLAGS.n_processes)]
chunk_sizes = len(all_files) // FLAGS.n_processes
for index in range(FLAGS.n_processes):
    if index == FLAGS.n_processes - 1:  # allocate ranges (last GPU takes remainders)
        start, end = index * chunk_sizes, len(all_files)
    else:
        start, end = index * chunk_sizes, (index + 1) * chunk_sizes
    filenames[index] = all_files[start:end]
    labels[index] = all_labels[start:end]

# run multiple processes
pool = multiprocessing.Pool(processes=FLAGS.n_processes)
pool.map(process, [(filenames[i], labels[i], FLAGS.out_dir, FLAGS.extension) for i in range(FLAGS.n_processes)])
