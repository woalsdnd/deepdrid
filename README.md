# **ISBI 2020 DeepDRiD challenge - VUNO EYE TEAM**  

## **Install Packages** (downloading each dataset needs to be consulted to individual data owners)    
- Requirements.txt files are under subchallenge1 and subchallenge3. Use them with the following command  
- ```pip install -r requirements.txt```   


## **Public Dataset** (downloading each dataset needs to be consulted to individual data owners)    
### **Labeled Data**  
- **Kaggle 2015: **  https://www.kaggle.com/c/diabetic-retinopathy-detection  
- **IDRiD: **  https://idrid.grand-challenge.org/  
- **APTOS: **  https://www.kaggle.com/c/aptos2019-blindness-detection  
- **DeepDRDRiD: **  https://isbi.deepdr.org/  
### **Unlabeled Data**  
- **AMD: **  https://amd.grand-challenge.org/  
- **PALM: **  https://palm.grand-challenge.org/  
- **REFUGE: **  https://refuge.grand-challenge.org/  
- **RIGA: **  https://deepblue.lib.umich.edu/data/concern/data_sets/3b591905z?locale=en  
- **MESSIDOR 2 : **  http://www.adcis.net/en/third-party/messidor2/  
- **MESSIDOR: **  http://www.adcis.net/en/third-party/messidor/  
- **ODIR: **  https://odir2019.grand-challenge.org/  
- **STARE: **  https://cecas.clemson.edu/~ahoover/stare/  
- **AV crossing: ** https://people.eng.unimelb.edu.au/thivun/projects/AV_nicking_quantification/   
- **DiaretDB: **  https://www.it.lut.fi/project/imageret/diaretdb1/  
- **APTOS (validation set): **  https://www.kaggle.com/c/aptos2019-blindness-detection  
- **E-ophtha: **  http://www.adcis.net/en/third-party/e-ophtha/  

## **TRAIN** (beware of paths for data)    
1. prepare images using task-specific pre-processing codes    
   - ** Subchallenge1 : ** subchallenge1/codes/crop_resize.py  
   - ** Subchallenge3 (center crop) : ** subchallenge3/preprocessing/crop_center_resize_subchallenge3.py 
   - ** Subchallenge3 (retrieve peripheral patches): ** subchallenge3/preprocessing/crop_peripheral_subchallenge3.py  
   - ** Subchallenge3 (compute RGB mean): ** subchallenge3/preprocessing/compute_mean.py   
   - ** Subchallenge3 (match RGB mean): ** subchallenge3/preprocessing/cconvert_regular2wide.py  
2. run training commands (followings are examples)    
   - **Pretraining network (at subchallenge1/codes or subchallenge3/codes)**  
    ```python train_labeled_data.py --config_file=../config/init_model_B4_1024x1024 ```  
   - **Loop for subchallenge1: (at subchallenge1/codes)**    
   Move pretrained network with filename 'weight_0epoch.h5', then    
    ```bash run run_train.sh```  
   - **Loop for subchallenge3: (at subchallenge3/codes)**   
    Move pretrained network with filename 'weight_0epoch.h5', then  
    ```bash run run_train.sh```  
   - **Patch training for subchallenge3: (at subchallenge3/codes)**   
    ```python inference_patches_wide.py --gpu_index=0,2 --img_dir=../../data/DeepDRiD/ultra-widefield-training/images/ --out_dir=../intermediate_outputs/train --phase=train```  
    ```python train_patches.py --gpu_index=0```   
   - **Optimize thresholds: **   
    - **Subchallenge1 (at subchallenge1/codes)**  
    ```python optimize_thresholds.py --pred_csv=../submission/onsite_regular_train/Training_result.csv  --gt_csv=../../data/DeepDRiD/regular-fundus-training/regular-fundus.csv  --img_type=wide```   
    Then, modify the thresholds in inference.py  
    - **Subchallenge3 (at subchallenge3/codes)**  
    ```python inference_patches_wide.py --gpu_index=0,2 --img_dir=../../data/DeepDRiD/ultra-widefield-training/images/ --out_dir=../intermediate_outputs/train --phase=optimize_thresholds```   
    ```python optimize_thresholds.py --pred_csv=../intermediate_outputs/train/Subchallenge3.csv  --gt_csv=../../data/DeepDRiD/ultra-widefield-training/ultra-widefield.csv  --img_type=wide```   
    Then, modify the thresholds in inference_patches_wide.py    

## **INFERENCE**   
1. prepare images in a original size  
2. run inference commands (followings are examples)    
   - **Subchallenge1 (at subchallenge1/codes):**  
    ```python inference.py --gpu_index=3 --img_dir=../../data/DeepDRiD/Onsite-Challenge/images/ --submission_version=onsite_regular  --img_type=regular```   
   - **Subchallenge3 (at subchallenge3/codes):**   
   ```python inference_patches_wide.py --gpu_index=0,2 --img_dir=../../data/DeepDRiD/Onsite-Challenge/wide-images/  --out_dir=../intermediate_outputs/onsite_key_patches --phase=test```  
   
   
## **MISC files**  
   - run_multigpu_weight2singlegpu_weight.sh : modify subchallenge1/codes/crop_resize.py  