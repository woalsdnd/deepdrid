import os
import numpy as np
import argparse
import pandas as pd
import scipy as sp

from functools import partial
from sklearn import metrics
from sklearn.metrics import cohen_kappa_score

import utils


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--pred_csv',
    type=str,
    required=True
)
parser.add_argument(
    '--gt_csv',
    type=str,
    required=True
)
parser.add_argument(
    '--img_type',
    type=str,
    choices=['regular', 'wide'],
    required=True
)
FLAGS, _ = parser.parse_known_args()

class OptimizedRounder(object):

    def __init__(self):
        self.coef_ = 0

    def _kappa_loss(self, coef, X, y):
        X_p = np.copy(X)
        for i, pred in enumerate(X_p):
            if pred < coef[0]:
                X_p[i] = 0
            elif pred >= coef[0] and pred < coef[1]:
                X_p[i] = 1
            elif pred >= coef[1] and pred < coef[2]:
                X_p[i] = 2
            elif pred >= coef[2] and pred < coef[3]:
                X_p[i] = 3
            else:
                X_p[i] = 4

        ll = metrics.cohen_kappa_score(y, X_p, weights='quadratic')
        return -ll

    def fit(self, X, y):
        loss_partial = partial(self._kappa_loss, X=X, y=y)
        initial_coef = [0.5, 1.5, 2.5, 3.5]
        self.coef_ = sp.optimize.minimize(loss_partial, initial_coef, method='nelder-mead')

    def predict(self, X, coef):
        X_p = np.copy(X)
        for i, pred in enumerate(X_p):
            if pred < coef[0]:
                X_p[i] = 0
            elif pred >= coef[0] and pred < coef[1]:
                X_p[i] = 1
            elif pred >= coef[1] and pred < coef[2]:
                X_p[i] = 2
            elif pred >= coef[2] and pred < coef[3]:
                X_p[i] = 3
            else:
                X_p[i] = 4
        return X_p

    def coefficients(self):
        return self.coef_['x']

# load dataframes
df_pred = pd.read_csv(FLAGS.pred_csv)
df_pred["image_id"] = df_pred["image_id"].map(lambda x:x.split(".")[0])
df_gt = pd.read_csv(FLAGS.gt_csv)
if FLAGS.img_type=="regular":
    df_gt["gt"] = df_gt.apply(lambda row: row["left_eye_DR_Level"] if "l" in row["image_id"] else row["right_eye_DR_Level"], axis=1)
elif FLAGS.img_type=="wide":
    df_gt["gt"] = df_gt["DR_level"]
    df_gt = df_gt.loc[df_gt["gt"]<5,:]
col_name = "DR_level" if FLAGS.img_type=="wide" else "DR_Level"
df_pred.loc[:,"img_lat"] = df_pred["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
df_merged = pd.merge(df_gt[["image_id","gt"]], df_pred, on="image_id")
df_pred_max = df_pred.groupby("img_lat")[col_name].apply(lambda x:np.max(x)).reset_index()
df_pred_max_merged = pd.merge(df_pred[["img_lat","image_id"]], df_pred_max, on="img_lat")
df_merged = pd.merge(df_gt[["image_id","gt"]], df_pred_max_merged, on="image_id")

# optimize thresholds
optR = OptimizedRounder()
optR.fit(df_merged[col_name], df_merged["gt"])
coefficients = optR.coefficients()
optimized_predictions = optR.predict(df_merged[col_name], coefficients)

# print results
print(coefficients)
print("===== before thresholds optimization =====")
print(utils.categorical_stats(df_merged["gt"], np.round(df_merged[col_name]), weighted_kappa=True))
print("===== after thresholds optimization =====")
print(utils.categorical_stats(df_merged["gt"], optimized_predictions, weighted_kappa=True))