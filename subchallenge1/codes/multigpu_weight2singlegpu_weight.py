# python3 multigpu_weight2singlegpu_weight.py --weight_file=../model/B4/iteration/overfit_val_weight_1epoch.h5
import os
import h5py
import argparse
import numpy as np

from model.efficient_net import EfficientNetB4, set_output

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--weight_file',
    type=str,
    required=True
)
FLAGS, _ = parser.parse_known_args()

os.environ['CUDA_VISIBLE_DEVICES'] = "-1"

network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=(1024,1024,3))
network = set_output(network)

with h5py.File(FLAGS.weight_file, "r") as fr:
    model_key = [cand for cand in fr.keys() if "model" in cand][0]
    all_weights = fr[model_key]
    dense_start_id = np.min([int(cand.replace("dense_", "")) for cand in all_weights.keys() if "dense" in cand])
    for layer_name, layer_weights in all_weights.items():
        print("load {}".format(layer_name))
        # Extract weights
        weights= [np.array(layer_weights[term]) if len(np.array(layer_weights[term])[np.isnan(np.array(layer_weights[term]))]) == 0 else None for term in layer_weights]
        if "bn" in layer_name: # bn
            assert len(weights) == 4
            weights = [weights[1], weights[0], weights[2], weights[3]]
        elif len(weights) > 1: # conv layer with bias
            weights = [weights[1], weights[0]]
        
        # modify layer_name for dense layers
        if "dense" in layer_name:
            curr_layer_id = int(layer_name.replace("dense_", ""))
            layer_name = "dense_{}".format(1+curr_layer_id-dense_start_id)
        
        # Load weights to model
        network.get_layer(layer_name).set_weights(weights)

# save weights
out_weight_path = FLAGS.weight_file.replace(".h5", "_single_gpu.h5")
network.save_weights(out_weight_path)