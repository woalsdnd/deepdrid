import sys
import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB2, EfficientNetB3, EfficientNetB4, set_output, compile_loss

from keras import backend as K
from keras.utils.training_utils import multi_gpu_model

def trainable_labels(df):
    list_aug = ["ori", "rot90", "rot180", "rot270", "color", "contrast", "brightness", "sharpness"]
    list_output_type = ["binary_0", "binary1", "binary2", "binary3", "reg"]
    binary_higher_threshold = 0.3
    binary_lower_threshold = 0.7
    reg_diff_threshold = 0.5
    label0_reg_threshold=reg_diff_threshold
    label1_reg_threshold1=1-reg_diff_threshold
    label1_reg_threshold2=1+reg_diff_threshold
    label2_reg_threshold1=2-reg_diff_threshold
    label2_reg_threshold2=2+reg_diff_threshold
    label3_reg_threshold1=3-reg_diff_threshold
    label3_reg_threshold2=3+reg_diff_threshold
    label4_reg_threshold=4-reg_diff_threshold
    
    # compute average
    for output_type in list_output_type:
        df.loc[:, output_type] = df.apply(lambda x: np.mean([x[aug + "_" + output_type] for aug in list_aug]), axis=1)
    df.loc[:,"max_diff"]=df.apply(lambda x: np.max(np.clip([x[aug + "_reg"] for aug in list_aug],0,4))-np.min(np.clip([x[aug + "_" + output_type] for aug in list_aug], 0,4)), axis=1)

    # select trainable labels    
    df_trainable_label0=df[(df["binary_0"]<binary_lower_threshold) & (df["binary1"]<binary_lower_threshold) & (df["binary2"]<binary_lower_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]<label0_reg_threshold) & (df["max_diff"]<0.5)]
    df_trainable_label1=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]<binary_lower_threshold) & (df["binary2"]<binary_lower_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]>=label1_reg_threshold1) & (df["reg"]<label1_reg_threshold2) & (df["max_diff"]<0.5)]
    df_trainable_label2=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]>binary_higher_threshold) & (df["binary2"]<binary_lower_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]>=label2_reg_threshold1) & (df["reg"]<label2_reg_threshold2) & (df["max_diff"]<0.5)]
    df_trainable_label3=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]>binary_higher_threshold) & (df["binary2"]>binary_higher_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]>=label3_reg_threshold1) & (df["reg"]<label3_reg_threshold2) & (df["max_diff"]<0.5)]
    df_trainable_label4=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]>binary_higher_threshold) & (df["binary2"]>binary_higher_threshold) & (df["binary3"]>binary_higher_threshold) & (df["reg"]>=label4_reg_threshold)]
    df = pd.concat([df_trainable_label0, df_trainable_label1, df_trainable_label2, df_trainable_label3, df_trainable_label4], ignore_index=True)

    return df

def label2binary(gt_labels):
    gt_step0 = np.array([0 if gt < 0.5 else 1 for gt in gt_labels])
    gt_step1 = np.array([0 if gt < 1.5 else 1 for gt in gt_labels])
    gt_step2 = np.array([0 if gt < 2.5 else 1 for gt in gt_labels])
    gt_step3 = np.array([0 if gt < 3.5 else 1 for gt in gt_labels])
    return gt_step0, gt_step1, gt_step2, gt_step3


def set_filenames_labels(dataset_name, fpaths_data, df_label):
    if "kaggle2015" in dataset_name:
        fid_col, label_col = "image", "level"
    elif "APTOS" in dataset_name:
        fid_col, label_col = "id_code", "diagnosis"
    elif "IDRiD" in dataset_name:
        fid_col, label_col = "Image name", "Retinopathy grade"
    elif "DeepDRiD" in dataset_name:
        df_label["level"] = df_label.apply(lambda row: row["left_eye_DR_Level"] if "l" in row["image_id"] else row["right_eye_DR_Level"], axis=1)
        fid_col, label_col = "image_id", "level"
    df_filepath_idcode = pd.DataFrame({"filepath": fpaths_data, fid_col: [os.path.basename(fpath).replace(".png", "") for fpath in fpaths_data]})
    df_merged = pd.merge(df_label, df_filepath_idcode, on=fid_col, how="inner")
    return list(df_merged["filepath"]), list(df_merged[label_col])


def set_output_multigpu_loss(network, loss_weight, lr_start_value, n_multigpu):
    new_network = set_output(network)
    if n_multigpu > 1:
        new_network = multi_gpu_model(new_network, gpus=n_multigpu)
    new_network = compile_loss(new_network, lr_start_value, loss_weight)
    return new_network


def set_trainable(netweork, val):
    network.trainable = val
    for l in network.layers:
        l.trainable = val


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
)
parser.add_argument(
    '--iter_loop',
    type=int,
    required=True
)
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
dir_training_input_data2 = config["Path"]["path_data2"]
dir_training_input_data3 = config["Path"]["path_data3"]
dir_training_input_label1 = config["Path"]["path_label1"]
dir_training_input_label2 = config["Path"]["path_label2"]
dir_training_input_label3 = config["Path"]["path_label3"]
list_dir_unlabeled_data = [dir_path.strip() for dir_path in config["Path"]["path_unlabeled"].split(",")] if "path_unlabeled" in config["Path"] else []
dir_val_input_data = config["Path"]["path_val_data"]
dir_val_input_label = config["Path"]["path_val_label"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log"))
model_type = config["Train"]["model_type"]
batch_size = int(config["Train"]["batch_size"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
weight_pseudo_label_branch = float(config["Train"]["weight_pseudo_label_branch"]) if "weight_pseudo_label_branch" in config["Train"] else None
n_multigpu = len(gpu_index.split(","))
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)

# split data
fpaths_val_data = utils.all_files_under(dir_val_input_data)
df_val_label = pd.read_csv(dir_val_input_label)
# set filenames and labels
fpaths_val_data, label_val_data = set_filenames_labels(dir_val_input_data, fpaths_val_data, df_val_label)
assert len(fpaths_val_data) == len(label_val_data)

# set iterators
val_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_val_data, label_val_data), None,
                                                       utils.fundus_classification_processing_func_val_DeepDRiD, batch_size, sample=False, replace=False, shared_array_shape=[input_size, ()], return_id=True)

# define network
# make imagenet-pretrained if not exists
if "imagenet_pretrained" in path_load_model and (not os.path.exists(path_load_model) or len(utils.all_files_under(path_load_model, extension="h5")) == 0):
    from keras.applications.resnet50 import ResNet50
    resnet = ResNet50(weights='imagenet')
    if model_type == "B2":
        network = EfficientNetB2(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB2(include_top=False, weights="imagenet", input_shape=input_size)
    elif model_type == "B3":
        network = EfficientNetB3(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB3(include_top=False, weights="imagenet", input_shape=input_size)
    elif model_type == "B4":
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB4(include_top=False, weights="imagenet", input_shape=input_size)
    network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    network = utils.copy_weights(network_imagenet_pretrained, network)
    utils.makedirs(path_load_model)
    network.save_weights(os.path.join(path_load_model, "imagenet_pretrained_weight.h5"))
else:
    if model_type == "B2":
        network = EfficientNetB2(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    elif model_type == "B3":
        network = EfficientNetB3(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    elif model_type == "B4":
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    
# compile network
network.load_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(FLAGS.iter_loop - 1)))
logger.info("weights loaded from {}".format(os.path.join(dir_save_model, "weight_{}epoch.h5".format(FLAGS.iter_loop - 1))))
network_data1 = set_output_multigpu_loss(network, 0, lr_start_value, n_multigpu)
network_data2 = set_output_multigpu_loss(network, 0, lr_start_value, n_multigpu)
network_data3 = set_output_multigpu_loss(network, 0, lr_start_value, n_multigpu)
network_data_pseudo_label = set_output_multigpu_loss(network, weight_pseudo_label_branch, lr_start_value, n_multigpu)
# set_trainable(network, False)
network_fit_to_validation = set_output_multigpu_loss(network, 1, lr_start_value, n_multigpu)
network_fit_to_validation.summary()
sys.stdout.flush()
network_data1.load_weights(os.path.join(dir_save_model, "network_data1_weight_{}epoch.h5".format(FLAGS.iter_loop - 1)))
network_data2.load_weights(os.path.join(dir_save_model, "network_data2_weight_{}epoch.h5".format(FLAGS.iter_loop - 1)))
network_data3.load_weights(os.path.join(dir_save_model, "network_data3_weight_{}epoch.h5".format(FLAGS.iter_loop - 1)))
network_data_pseudo_label.load_weights(os.path.join(dir_save_model, "network_data_pseudo_label_weight_{}epoch.h5".format(FLAGS.iter_loop - 1)))
logger.info("weights loaded from {}".format(os.path.join(dir_save_model, "network_data1_weight_{}epoch.h5".format(FLAGS.iter_loop - 1))))
logger.info("weights loaded from {}".format(os.path.join(dir_save_model, "network_data2_weight_{}epoch.h5".format(FLAGS.iter_loop - 1))))
logger.info("weights loaded from {}".format(os.path.join(dir_save_model, "network_data3_weight_{}epoch.h5".format(FLAGS.iter_loop - 1))))
logger.info("weights loaded from {}".format(os.path.join(dir_save_model, "network_data_pseudo_label_weight_{}epoch.h5".format(FLAGS.iter_loop - 1))))

n_val_fitting_epochs = 30
# select images to overfit
list_fpaths, list_gt, list_pred_data1, list_pred_data2, list_pred_data3, list_pred_pseudo = [], [], [], [], [], []
for data, list_arr in val_batch_fetcher:
    fpaths, _ = data
    # list_arr = [img, label]
    preds0_data1, preds1_data1, preds2_data1, preds3_data1, pred_reg_data1 = network_data1.predict(list_arr[0])
    preds0_data2, preds1_data2, preds2_data2, preds3_data2, pred_reg_data2 = network_data2.predict(list_arr[0])
    preds0_data3, preds1_data3, preds2_data3, preds3_data3, pred_reg_data3 = network_data3.predict(list_arr[0])    
    preds0_pseudo, preds1_pseudo, preds2_pseudo, preds3_pseudo, pred_reg_pseudo = network_data_pseudo_label.predict(list_arr[0])
    utils.stack_list(head1=list_gt, tail1=list(list_arr[1]), head2=list_pred_data1, tail2=list(pred_reg_data1[:,0]),
                    head3=list_pred_data2, tail3=list(pred_reg_data2[:,0]), head4=list_pred_data3, tail4=list(pred_reg_data3[:,0]), 
                    head5=list_pred_pseudo, tail5=list(pred_reg_pseudo[:,0]), head6=list_fpaths, tail6=list(fpaths))
df_val_inference = pd.DataFrame({"fpath": list_fpaths, "image_id":[os.path.basename(fp).split(".")[0] for fp in list_fpaths],
                                    "network_data1": list_pred_data1,
                                    "network_data2": list_pred_data2, "network_data3":list_pred_data3, 
                                    "network_pseudo":list_pred_pseudo, "label":list_gt})
df_val_inference.loc[:,"img_lat"] = df_val_inference["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
df_selected_fpath = df_val_inference.groupby("img_lat").apply(lambda row:row["fpath"][int(np.round(1.*(np.argmax(row["network_data1"])+np.argmax(row["network_data2"])+np.argmax(row["network_data3"])+np.argmax(row["network_pseudo"]))/4))]).reset_index(name='selected_fpath')
df_val_inference = pd.merge(df_val_inference, df_selected_fpath, on="img_lat")
df_diff_data1 = df_val_inference.groupby("img_lat")["network_data1"].apply(lambda x:np.max(np.clip(x,0,4))-np.min(np.clip(x,0,4))).reset_index(name='data1_diff')
df_diff_data2 = df_val_inference.groupby("img_lat")["network_data2"].apply(lambda x:np.max(np.clip(x,0,4))-np.min(np.clip(x,0,4))).reset_index(name='data2_diff')
df_diff_data3 = df_val_inference.groupby("img_lat")["network_data3"].apply(lambda x:np.max(np.clip(x,0,4))-np.min(np.clip(x,0,4))).reset_index(name='data3_diff')
df_diff_pseudo = df_val_inference.groupby("img_lat")["network_pseudo"].apply(lambda x:np.max(np.clip(x,0,4))-np.min(np.clip(x,0,4))).reset_index(name='pseudo_diff')
df_val_inference = pd.merge(df_val_inference, df_diff_data1, on="img_lat")
df_val_inference = pd.merge(df_val_inference, df_diff_data2, on="img_lat")
df_val_inference = pd.merge(df_val_inference, df_diff_data3, on="img_lat")
df_val_inference = pd.merge(df_val_inference, df_diff_pseudo, on="img_lat")
df_val_inference.to_csv(os.path.join(dir_experimental_result, "val_prediction_epoch{}.csv".format(FLAGS.iter_loop)), index=False)
# fitting to validation set
df_close_pred_fpath = df_val_inference[(df_val_inference["data1_diff"]<0.5) & (df_val_inference["data2_diff"]<0.5) & (df_val_inference["data3_diff"]<0.5) & (df_val_inference["pseudo_diff"]<0.5)]
df_val_inference = df_val_inference[df_val_inference["fpath"].isin(list(df_val_inference["selected_fpath"])+list(df_close_pred_fpath["fpath"]))]
selected_fpaths_val_data, selected_label_val_data = list(df_val_inference["selected_fpath"]), list(df_val_inference["label"])
assert len(selected_fpaths_val_data) == len(selected_label_val_data)
logger.info("# val_overfit_data: {}".format(len(selected_fpaths_val_data)))
_, sample_weight_val_data = utils.balanced_class_weights(selected_label_val_data)
val_fitting_batch_fetcher = iterator_shared_array.BatchFetcher((selected_fpaths_val_data, selected_label_val_data), [sample_weight_val_data],utils.fundus_classification_processing_func_train_DeepDRiD, batch_size, sample=True, replace=True, shared_array_shape=[input_size, ()])
dict_fitting_val = {"{}_val".format(metric): [] for metric in ["loss_total", "loss0", "loss1", "loss2", "loss3", "loss_reg", "acc0", "acc1", "acc2", "acc3"]}
for val_fitting_epoch in range(n_val_fitting_epochs):
    for list_arr in val_fitting_batch_fetcher:
        # list_arr = [img, label]
        # tensorboard.draw_imgs("Validation Fitting Image", 0, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        gt_step0_val, gt_step1_val, gt_step2_val, gt_step3_val = label2binary(list_arr[1])
        loss_total_val, loss0_val, loss1_val, loss2_val, loss3_val, loss_reg_val, acc0_val, acc1_val, acc2_val, acc3_val, _ = network_fit_to_validation.train_on_batch(list_arr[0], [gt_step0_val, gt_step1_val, gt_step2_val, gt_step3_val, list_arr[1]])
        for metric in ["loss_total", "loss0", "loss1", "loss2", "loss3", "loss_reg", "acc0", "acc1", "acc2", "acc3"]:
            varname = "{}_val".format(metric)
            dict_fitting_val[varname] += [locals()[varname]] * len(list_arr[0])
    train_metrics = {key: np.mean(val) for key, val in dict_fitting_val.items()}
    utils.log_summary(logger, phase="val_fitting", epoch="{}_{}".format(FLAGS.iter_loop, val_fitting_epoch), **train_metrics)
    if val_fitting_epoch==int(n_val_fitting_epochs * 0.9): # reduce lr at the last 10% epoch
        K.set_value(network_fit_to_validation.optimizer.lr, K.eval(network_fit_to_validation.optimizer.lr) * 0.1)
network_fit_to_validation.save_weights(os.path.join(dir_save_model, "overfit_val_weight_{}epoch.h5".format(FLAGS.iter_loop)))
# compute metrics
list_fpaths, list_gt, list_pred = [], [], []
for data, list_arr in val_batch_fetcher:
    fpaths, _ = data
    # list_arr = [img, label]
    preds0, preds1, preds2, preds3, pred_reg = network_fit_to_validation.predict(list_arr[0])    
    utils.stack_list(head1=list_gt, tail1=list(list_arr[1]), head2=list_pred, tail2=list(utils.float2class(pred_reg[:,0], 0, 4)), head3=list_fpaths, tail3=list(fpaths))
df = pd.DataFrame({"image_id":[os.path.basename(fpath).split(".")[0] for fpath in list_fpaths], "pred":list_pred, "gt":list_gt})
df.loc[:,"img_lat"] = df["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
df_pred_max = df.groupby("img_lat")["pred"].apply(lambda x:np.max(x)).reset_index()
df_pred_max_merged = pd.merge(df[["img_lat","image_id", "gt"]], df_pred_max, on="img_lat")
val_metrics = utils.categorical_stats(df_pred_max_merged["gt"], df_pred_max_merged["pred"], weighted_kappa=True)
utils.log_summary(logger, phase="after val overfitting", epoch=FLAGS.iter_loop, **val_metrics)
