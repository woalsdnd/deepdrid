# python inference.py --gpu_index=0 --img_dir=../../data/DeepDRiD/regular-fundus-training/resized_1024x1024/ --submission_version=epoch1_train_check/  --img_type=regular
# python inference.py --gpu_index=0,2 --img_dir=../../data/DeepDRiD/Online-Challenge/ultra-widefield_resized/ --submission_version=debug --img_type=wide
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils

from model.efficient_net import EfficientNetB4, set_output
from keras.models import Model
from keras.utils.training_utils import multi_gpu_model

def threshold(val):
    threhsolds = [0.53088379, 1.50263672, 2.29016113, 3.59587402]
    if val < threhsolds[0]:
        label = 0
    elif val >= threhsolds[0] and val < threhsolds[1]:
        label = 1
    elif val >= threhsolds[1] and val < threhsolds[2]:
        label = 2
    elif val >= threhsolds[2] and val < threhsolds[3]:
        label = 3
    else:
        label = 4
    return label

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--submission_version',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_type',
    type=str,
    choices=['regular', 'wide'],
    required=True
)
FLAGS, _ = parser.parse_known_args()

# misc params
if FLAGS.img_type=="regular":
    list_weight_path = ["/home/vuno/development/DeepDRiD/model/B4/regular_selected/overfit_val_weight_{}epoch_single_gpu.h5".format(epoch) for epoch in range(1,11)]
else:
    list_weight_path = ["/home/vuno/development/DeepDRiD/model/B4/iteration_ultra-wide/overfit_val_weight_1epoch.h5"]
input_size = (1024, 1024, 3)  # (h,w)
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
n_gpus = len(FLAGS.gpu_index.split(","))
out_dir_home="/home/vuno/development/DeepDRiD/submission/{}".format(FLAGS.submission_version)
utils.makedirs(out_dir_home)

# load a network
list_network = []
for weight_path in list_weight_path:
    network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    network = set_output(network)
    if n_gpus>1:
        network = multi_gpu_model(network, gpus=n_gpus)
    network.load_weights(weight_path)
    print("model loaded from {}".format(weight_path))
    list_network.append(network)

# run inference
fpaths = utils.all_files_under(FLAGS.img_dir)
list_pred_individual, list_pred = [[] for _ in range(len(list_network))], []
for fpath in fpaths:
    img = np.array(Image.open(fpath))
    resized_img = img if img.shape == input_size else utils.crop_resize(img, input_size[0], input_size[1])
    if resized_img is None: # ungradable to 0
        list_pred.append(np.mean(0))
        continue
    resized_img = 1.*resized_img / 255
    network_input = np.expand_dims(resized_img, axis=0)
    outputs=[]
    for index, network in enumerate(list_network):
        pred = np.clip(network.predict(network_input)[-1][0,0], 0, 4)
        outputs.append(pred)
        list_pred_individual[index].append(threshold(pred))
    final_pred = threshold(np.mean(outputs))
    list_pred.append(final_pred)
    print(fpath, final_pred)

# save csv (take max for each laterality)
col_name = "DR_level" if FLAGS.img_type=="wide" else "DR_Level"
filename = "Training_result.csv" if "train" in FLAGS.submission_version else "Offsite_submission.csv"
df = pd.DataFrame({"image_id":[os.path.basename(fpath).split(".")[0] for fpath in fpaths], col_name:list_pred})
if "train" in FLAGS.submission_version: # run on train set
    df[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename), index=False)
    for index in range(len(list_network)):
        df = pd.DataFrame({"image_id":[os.path.basename(fpath).split(".")[0] for fpath in fpaths], col_name:list_pred_individual[index]})
        df[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename.replace(".csv", "{}epoch_network.csv".format(index))), index=False)
elif "onsite" in FLAGS.submission_version: # final submission for onsite challenge
    csv_submission_fname = "Challenge3_upload.csv" if FLAGS.img_type=="wide" else  "Challenge1_upload.csv" 
    df_submission_template = pd.read_csv(os.path.join("/home/vuno/development/data/DeepDRiD/Onsite-Challenge", csv_submission_fname))
    df.loc[:,"img_lat"] = df["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
    df_pred_max = df.groupby("img_lat")[col_name].apply(lambda x:np.max(x)).reset_index()
    df_pred_max_merged = pd.merge(df[["img_lat","image_id"]], df_pred_max, on="img_lat")
    df_pred_max_merged = pd.merge(df_submission_template[["image_id"]], df_pred_max_merged, on="image_id")
    df_pred_max_merged[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename), index=False)
    if FLAGS.img_type=="regular":
        for index in range(len(list_network)):
            df = pd.DataFrame({"image_id":[os.path.basename(fpath).split(".")[0] for fpath in fpaths], col_name:list_pred_individual[index]})
            df.loc[:,"img_lat"] = df["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
            df_pred_max = df.groupby("img_lat")[col_name].apply(lambda x:np.max(x)).reset_index()
            df_pred_max_merged = pd.merge(df[["img_lat","image_id"]], df_pred_max, on="img_lat")
            df_pred_max_merged = pd.merge(df_submission_template[["image_id"]], df_pred_max_merged, on="image_id")
            df_pred_max_merged[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename.replace(".csv", "{}epoch_network.csv".format(index))), index=False)
else: # submission for offsite challenge
    csv_submission_fname = "Challenge3_upload.csv" if FLAGS.img_type=="wide" else  "Challenge1_upload.csv" 
    df_submission_template = pd.read_csv(os.path.join("/home/vuno/development/data/DeepDRiD/Online-Challenge", csv_submission_fname))
    df.loc[:,"img_lat"] = df["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
    df_pred_max = df.groupby("img_lat")[col_name].apply(lambda x:np.max(x)).reset_index()
    df_pred_max_merged = pd.merge(df[["img_lat","image_id"]], df_pred_max, on="img_lat")
    df_pred_max_merged = pd.merge(df_submission_template[["image_id"]], df_pred_max_merged, on="image_id")
    df_pred_max_merged[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename), index=False)
    if FLAGS.img_type=="regular":
        for index in range(len(list_network)):
            df = pd.DataFrame({"image_id":[os.path.basename(fpath).split(".")[0] for fpath in fpaths], col_name:list_pred_individual[index]})
            df.loc[:,"img_lat"] = df["image_id"].map(lambda x:x.split("_")[0]+"_"+x.split("_")[1][0])
            df_pred_max = df.groupby("img_lat")[col_name].apply(lambda x:np.max(x)).reset_index()
            df_pred_max_merged = pd.merge(df[["img_lat","image_id"]], df_pred_max, on="img_lat")
            df_pred_max_merged = pd.merge(df_submission_template[["image_id"]], df_pred_max_merged, on="image_id")
            df_pred_max_merged[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename.replace(".csv", "{}epoch_network.csv".format(index))), index=False)
    # df_pred_mean = df.groupby("img_lat")[col_name].apply(lambda x:np.mean(x)).reset_index()
    # df_pred_mean_merged = pd.merge(df[["img_lat","image_id"]], df_pred_mean, on="img_lat")
    # df_pred_mean_merged[["image_id", col_name]].to_csv(os.path.join(out_dir_home, filename), index=False)