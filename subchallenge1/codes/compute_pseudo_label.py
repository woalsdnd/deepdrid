import sys
import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB2, EfficientNetB3, EfficientNetB4, set_output, compile_loss

from keras import backend as K
from keras.utils.training_utils import multi_gpu_model

def trainable_labels(df):
    list_aug = ["ori", "rot90", "rot180", "rot270", "color", "contrast", "brightness", "sharpness"]
    list_output_type = ["binary_0", "binary1", "binary2", "binary3", "reg"]
    binary_higher_threshold = 0.3
    binary_lower_threshold = 0.7
    reg_diff_threshold = 0.5
    label0_reg_threshold=reg_diff_threshold
    label1_reg_threshold1=1-reg_diff_threshold
    label1_reg_threshold2=1+reg_diff_threshold
    label2_reg_threshold1=2-reg_diff_threshold
    label2_reg_threshold2=2+reg_diff_threshold
    label3_reg_threshold1=3-reg_diff_threshold
    label3_reg_threshold2=3+reg_diff_threshold
    label4_reg_threshold=4-reg_diff_threshold
    
    # compute average
    for output_type in list_output_type:
        df.loc[:, output_type] = df.apply(lambda x: np.mean([x[aug + "_" + output_type] for aug in list_aug]), axis=1)
    df.loc[:,"max_diff"]=df.apply(lambda x: np.max(np.clip([x[aug + "_reg"] for aug in list_aug],0,4))-np.min(np.clip([x[aug + "_" + output_type] for aug in list_aug], 0,4)), axis=1)

    # select trainable labels    
    df_trainable_label0=df[(df["binary_0"]<binary_lower_threshold) & (df["binary1"]<binary_lower_threshold) & (df["binary2"]<binary_lower_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]<label0_reg_threshold) & (df["max_diff"]<0.5)]
    df_trainable_label1=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]<binary_lower_threshold) & (df["binary2"]<binary_lower_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]>=label1_reg_threshold1) & (df["reg"]<label1_reg_threshold2) & (df["max_diff"]<0.5)]
    df_trainable_label2=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]>binary_higher_threshold) & (df["binary2"]<binary_lower_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]>=label2_reg_threshold1) & (df["reg"]<label2_reg_threshold2) & (df["max_diff"]<0.5)]
    df_trainable_label3=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]>binary_higher_threshold) & (df["binary2"]>binary_higher_threshold) & (df["binary3"]<binary_lower_threshold) & (df["reg"]>=label3_reg_threshold1) & (df["reg"]<label3_reg_threshold2) & (df["max_diff"]<0.5)]
    df_trainable_label4=df[(df["binary_0"]>binary_higher_threshold) & (df["binary1"]>binary_higher_threshold) & (df["binary2"]>binary_higher_threshold) & (df["binary3"]>binary_higher_threshold) & (df["reg"]>=label4_reg_threshold)]
    df = pd.concat([df_trainable_label0, df_trainable_label1, df_trainable_label2, df_trainable_label3, df_trainable_label4], ignore_index=True)

    return df

def label2binary(gt_labels):
    gt_step0 = np.array([0 if gt < 0.5 else 1 for gt in gt_labels])
    gt_step1 = np.array([0 if gt < 1.5 else 1 for gt in gt_labels])
    gt_step2 = np.array([0 if gt < 2.5 else 1 for gt in gt_labels])
    gt_step3 = np.array([0 if gt < 3.5 else 1 for gt in gt_labels])
    return gt_step0, gt_step1, gt_step2, gt_step3


def set_filenames_labels(dataset_name, fpaths_data, df_label):
    if "kaggle2015" in dataset_name:
        fid_col, label_col = "image", "level"
    elif "APTOS" in dataset_name:
        fid_col, label_col = "id_code", "diagnosis"
    elif "IDRiD" in dataset_name:
        fid_col, label_col = "Image name", "Retinopathy grade"
    elif "DeepDRiD" in dataset_name:
        df_label["level"] = df_label.apply(lambda row: row["left_eye_DR_Level"] if "l" in row["image_id"] else row["right_eye_DR_Level"], axis=1)
        fid_col, label_col = "image_id", "level"
    df_filepath_idcode = pd.DataFrame({"filepath": fpaths_data, fid_col: [os.path.basename(fpath).replace(".png", "") for fpath in fpaths_data]})
    df_merged = pd.merge(df_label, df_filepath_idcode, on=fid_col, how="inner")
    return list(df_merged["filepath"]), list(df_merged[label_col])


def set_output_multigpu_loss(network, loss_weight, lr_start_value, n_multigpu):
    new_network = set_output(network)
    if n_multigpu > 1:
        new_network = multi_gpu_model(new_network, gpus=n_multigpu)
    new_network = compile_loss(new_network, lr_start_value, loss_weight)
    return new_network


def set_trainable(netweork, val):
    network.trainable = val
    for l in network.layers:
        l.trainable = val


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
)
parser.add_argument(
    '--iter_loop',
    type=int,
    required=True
)
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
dir_training_input_data2 = config["Path"]["path_data2"]
dir_training_input_data3 = config["Path"]["path_data3"]
dir_training_input_label1 = config["Path"]["path_label1"]
dir_training_input_label2 = config["Path"]["path_label2"]
dir_training_input_label3 = config["Path"]["path_label3"]
list_dir_unlabeled_data = [dir_path.strip() for dir_path in config["Path"]["path_unlabeled"].split(",")] if "path_unlabeled" in config["Path"] else []
dir_val_input_data = config["Path"]["path_val_data"]
dir_val_input_label = config["Path"]["path_val_label"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log"))
model_type = config["Train"]["model_type"]
batch_size = int(config["Train"]["batch_size"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
weight_pseudo_label_branch = float(config["Train"]["weight_pseudo_label_branch"]) if "weight_pseudo_label_branch" in config["Train"] else None
n_multigpu = len(gpu_index.split(","))
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

assert weight_pseudo_label_branch is not None

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)

# split data
fpaths_data1 = utils.all_files_under(dir_training_input_data1)
fpaths_data2 = utils.all_files_under(dir_training_input_data2)
fpaths_data3 = utils.all_files_under(dir_training_input_data3)
fpaths_unlabeled = []
for dir_unlabeled_data in list_dir_unlabeled_data:
    fpaths_unlabeled += utils.all_files_under(dir_unlabeled_data)
fpaths_val_data = utils.all_files_under(dir_val_input_data)
df_label_data1 = pd.read_csv(dir_training_input_label1)
df_label_data2 = pd.read_csv(dir_training_input_label2)
df_label_data3 = pd.read_csv(dir_training_input_label3)
df_val_label = pd.read_csv(dir_val_input_label)
# set filenames and labels
fpaths_data1, label_data1 = set_filenames_labels(dir_training_input_data1, fpaths_data1, df_label_data1)
fpaths_data2, label_data2 = set_filenames_labels(dir_training_input_data2, fpaths_data2, df_label_data2)
fpaths_data3, label_data3 = set_filenames_labels(dir_training_input_data3, fpaths_data3, df_label_data3)
fpaths_val_data, label_val_data = set_filenames_labels(dir_val_input_data, fpaths_val_data, df_val_label)
assert len(fpaths_data1) == len(label_data1)
assert len(fpaths_data2) == len(label_data2)
assert len(fpaths_data3) == len(label_data3)
assert len(fpaths_val_data) == len(label_val_data)
logger.info("# data1: {}".format(len(fpaths_data1)))
logger.info("# data2: {}".format(len(fpaths_data2)))
logger.info("# data3: {}".format(len(fpaths_data3)))

# set iterators
n_weak_augment = 8
fpaths_all_data = fpaths_data1 + fpaths_data2 + fpaths_data3 + fpaths_unlabeled
logger.info("# all data: {}".format(len(fpaths_all_data)))
pseudo_label_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_all_data, ), None, utils.fundus_classification_processing_func_weak_aug_DeepDRiD,
                                                                batch_size, sample=False, replace=False, shared_array_shape=[(n_weak_augment,) + input_size], return_id=True)


# define network
# make imagenet-pretrained if not exists
if "imagenet_pretrained" in path_load_model and (not os.path.exists(path_load_model) or len(utils.all_files_under(path_load_model, extension="h5")) == 0):
    from keras.applications.resnet50 import ResNet50
    resnet = ResNet50(weights='imagenet')
    if model_type == "B2":
        network = EfficientNetB2(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB2(include_top=False, weights="imagenet", input_shape=input_size)
    elif model_type == "B3":
        network = EfficientNetB3(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB3(include_top=False, weights="imagenet", input_shape=input_size)
    elif model_type == "B4":
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network_imagenet_pretrained = EfficientNetB4(include_top=False, weights="imagenet", input_shape=input_size)
    network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    network = utils.copy_weights(network_imagenet_pretrained, network)
    utils.makedirs(path_load_model)
    network.save_weights(os.path.join(path_load_model, "imagenet_pretrained_weight.h5"))
else:
    if model_type == "B2":
        network = EfficientNetB2(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    elif model_type == "B3":
        network = EfficientNetB3(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    elif model_type == "B4":
        network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)

# compile network
network_fit_to_validation = set_output_multigpu_loss(network, 1, lr_start_value, n_multigpu)
network_fit_to_validation.load_weights(os.path.join(dir_save_model, "overfit_val_weight_{}epoch.h5".format(FLAGS.iter_loop)))
logger.info("weights loaded from {}".format(os.path.join(dir_save_model, "overfit_val_weight_{}epoch.h5".format(FLAGS.iter_loop))))

# inference loop
list_weak_aug = ["ori", "rot90", "rot180", "rot270", "color", "contrast", "brightness", "sharpness"]
list_fpath = []
dict_binary0, dict_binary1, dict_binary2, dict_binary3, dict_reg = [{key:[] for key in list_weak_aug} for _ in range(5)]
for fpaths, arrs in pseudo_label_batch_fetcher:
    # fpaths => [(fpath1 fpath2 ...)]
    # arrs => [(bs, 8, h, w, d)]
    fpaths, arrs = fpaths[0], arrs[0]
    # tensorboard.draw_imgs("Weak Augmentation Image", 0, (np.concatenate([arr for arr in arrs], axis=0) * 255).astype(np.uint8), plot_once=True)
    
    for batch_index, img_weak_aug in enumerate(arrs):
        list_fpath.append(fpaths[batch_index])
        preds0, preds1, preds2, preds3, pred_reg = network_fit_to_validation.predict(img_weak_aug)
        for i in range(len(list_weak_aug)):
            key_aug=list_weak_aug[i]
            utils.stack_list(head1=dict_binary0[key_aug], tail1=list(preds0[i]), head2=dict_binary1[key_aug], tail2=list(preds1[i]), head3=dict_binary2[key_aug], tail3=list(
                preds2[i]), head4=dict_binary3[key_aug], tail4=list(preds3[i]), head5=dict_reg[key_aug], tail5=list(pred_reg[i]))
dict_inference_results = {"filepath":list_fpath}
for task_name, dict_task in zip(["binary_0", "binary1", "binary2", "binary3", "reg"],[dict_binary0, dict_binary1, dict_binary2, dict_binary3, dict_reg]):
    dict_inference_results.update({"{}_{}".format(aug_name, task_name):val for aug_name, val in dict_task.items()})
df_inference = pd.DataFrame(dict_inference_results)
df_inference.to_csv(os.path.join(dir_experimental_result, "weak_aug_inference_results_epoch{}.csv".format(FLAGS.iter_loop)), index=False)
